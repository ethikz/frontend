[![Known Vulnerabilities](https://snyk.io/test/npm/name/badge.svg)](https://snyk.io/test/npm/name)

### I am getting errors when trying to install node modules from `npm install`
Chances are there are permission issues, meaning you installed Node with `sudo`.  This causes a lot of issues when trying to install node modules from within projects.  What you need to do to fix it is:

1. [Download Node](https://nodejs.org/en/download) and install it by going through the prompts.

2. Open the application ***Terminal***.

3. Paste this command:
  ```
  export PATH=/usr/local/bin:/usr/local/sbin:/bin:/usr/bin:/sbin:/usr/sbin:./node/bin:./node_modules/.bin:~/.npm-global/bin:/bin:$HOME/.rvm/bin:$HOME/.rvm/gems/ruby-2.1.5/bin:$PATH
  ```

4. [Download Homebrew](http://brew.sh) and install it.
  - If you don't want to go to the site then with ***Terminal*** open, paste the following command:
    `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

5. [Download RVM](https://rvm.io/rvm/install) and install it.
  - If you don't want to go to the site then with ***Terminal*** open, paste the following command:
    `\curl -sSL https://get.rvm.io | bash -s stable --ruby`

6. Install Ruby 2.1.5 by pasting this into ***Terminal***, `rvm install 2.1.5`

7. You'll need to change permissions for folders as well which you can do by pasting the following commands:
  `sudo chown -R $(whoami) ~/.rvm && sudo chown -R $(whoami) ~/.gem`

8. With terminal open, copy and paste `source ~/.profile` or if it doesn't exist type `source ~/.bash_profile`.  This reloads your profile so you don't have to close and reopen terminal after all the commands.

9. Reinstall node modules by typing `npm install` in the project directory.
