/* eslint-disable no-invalid-this */
/* eslint-disable operator-linebreak */

import $ from 'jquery';

let Search = ( function() {
  const _private = {
    cacheDom: () => {
      _private.$search = $( '.js-search__input' );
      _private.$searchResults = $( '.js-search__results' );
      _private.$close = $( '.close-button' );
      _private.$searchList = $( '#search-list' );
      _private.$key = 0;
      _private.$items = [];
    },

    increment: ( n ) => {
      n++;
      return n;
    },

    bindEvents: () => {
      $.ajax( 'https://dog.ceo/api/breeds/list' ).done( function( data ) {
        let listItems = data.message;

        listItems.forEach( function( value, i ) {
          _private.$searchList.append( $( '<option value="' + value + '"></option>' ) );
        });
      });

      _private.$search.on( 'keyup', function( e ) {
        if ( e.which == 13 && _private.$search.val() != '' ) {
          _private.checkForDuplicate( _private.$search.val(), _private.$key );
        }
      });

      _private.$search.on( 'keydown', function( e ) {
        $( 'li' ).removeClass( 'hover' );
      });
    },

    checkForDuplicate: ( $searchTerm, $key ) => {
      if ( _private.$items.find( item => item.term === $searchTerm ) != undefined ) {
        let termKey = _private.$items.find( item => item.term === $searchTerm ).id;

        $( 'li[data-key="' + termKey + '"]' ).addClass( 'hover' );
      } else {
        _private.storeItem( $searchTerm, $key );
        _private.$key = _private.increment( $key );
      }
    },

    storeItem: ( $searchTerm, $key ) => {
      _private.$items.push({
        id: $key,
        term: $searchTerm
      });

      _private.appendItem( $searchTerm, $key );
    },

    appendItem: ( $searchTerm, $key ) => {
      let today = new Date();
      let dd = today.getDate();
      let mm = today.getMonth();
      let yyyy = today.getFullYear();
      let min = ( `0${ today.getMinutes() }` ).slice( -2 );
      let hh = today.getHours();

      if ( dd < 10 ) {
        dd = '0' + dd;
      }

      let date = yyyy + '-' + mm + '-' + dd;
      let time = hh + ':' + min;

      let listItem = '<li data-key="' + $key + '">'
        + '<div>'
          + '<span class="search__results-term">' + $searchTerm + '</span>'
        + '</div>'
        + '<div>'
          + '<span class="search__results-date-time">' + date + ' @ ' + time + '</span>'
        + '</div>'
        + '<div>'
          + '<button class="close-button" role="img" aria-label="Close">'
            + '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" class="close-button__icon">'
              + '<g id="Layer_2" data-name="Layer 2">'
                + '<g id="Layer_1-2" data-name="Layer 1">'
                  + '<path d="M50,0a50,50,0,1,0,50,50A50,50,0,0,0,50,0Zm0,94.2A44.2,44.2,0,1,1,94.2,50,44.21,44.21,0,0,1,50,94.2Z" />'
                  + '<path d="M50,54.1l9.4,9.4" />'
                  + '<path d="M59.4,36.5,50,45.9l-9.4-9.4a2.9,2.9,0,1,0-4.1,4.1L45.9,50l-9.4,9.4a2.9,2.9,0,1,0,4.1,4.1L50,54.1l9.4,9.4a2.9,2.9,0,1,0,4.1-4.1L54.1,50l9.4-9.4a3,3,0,0,0,0-4.1A2.78,2.78,0,0,0,59.4,36.5Z" />'
                + '</g>'
              + '</g>'
            + '</svg>'
          + '</button>'
        + '</div>'
      + '</li>';

      $( '.search__results' ).removeClass( 'is-hidden' ).append( listItem );

      _private.$search.val( '' );
    },

    removeItem: () => {
      _private.$searchResults.on( 'click', 'button', function() {
        _private.$items.splice( _private.$items.indexOf( $( this ).parents( 'li' ).data( 'key' ) - 1 ) );

        $( this ).parents( 'li' ).remove();

        if ( $( '.js-search__results li' ).length <= 0 ) {
          $( '.search__results' ).addClass( 'is-hidden' );

          _private.$key = 0;
        }
      });
    },

    init: () => {
      _private.cacheDom();
      _private.bindEvents();
      _private.removeItem();
    }
  };

  const _public = {
    init: _private.init,
    bindEvents: _private.bindEvents,
    removeItem: _private.removeItem
  };

  return _public;
})();

module.exports = Search;
