// Vendor
import Modernizr from 'modernizr';
import $ from 'jquery';

// Global functions
import webfont from 'functions/webfont.js';

// Components
import Search from 'components/search/search.js';


// Init
$( document ).ready( () => {
  webfont();
  Search.init();
});
