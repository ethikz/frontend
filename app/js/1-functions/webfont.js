export default function webfont() {
  let WebFontConfig = {
    google: {
      families: ['Open+Sans:400,300,600,700:latin']
    }
  };

  ( ( () => {
    const wf = document.createElement( 'script' );
    const s = document.getElementsByTagName( 'script' )[0];

    wf.src = `${'https:' == document.location.protocol ? 'https' : 'http'}://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js`;
    wf.type = 'text/javascript';
    wf.async = 'true';
    s.parentNode.insertBefore( wf, s );
  }) ) ();
}
