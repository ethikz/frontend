/* eslint operator-linebreak: 0 */
/* eslint no-useless-concat: 0 */

const gulp = require( 'gulp' );
const runSequence = require( 'run-sequence' );
const gulpRequireTasks = require( 'gulp-require-tasks' );
const requireDir = require( 'require-dir' );
const gutil = require( 'gulp-util' );
const chalk = require( 'chalk' );
const config = require( './gulp-config.json' );

requireDir( './gulp-tasks', {
  recurse: true
});

global.mapError = function( err ) {
  if ( err.fileName ) {
    // Regular error
    gutil.log( chalk.red( err.name )
      + ': ' + chalk.yellow( err.fileName.replace( __dirname + 'app/js/', '' ) )
      + ': ' + 'Line ' + chalk.magenta( err.lineNumber )
      + ' & ' + 'Column ' + chalk.magenta( err.columnNumber || err.column )
      + ': ' + chalk.blue( err.description ) );
  } else {
    // Browserify error..
    gutil.log( chalk.red( err.name ) + ': ' + chalk.yellow( err.message ) );
  }
}

gulpRequireTasks({
  passGulp: true
});


// Tasks
// =======================================================
gulp.task( 'default', function( cb ) {
  return runSequence( 'clean', 'html', 'stats-analysis:scss-lint', 'scss', 'stats-analysis:retire', 'stats-analysis:js-lint', 'javascript', 'optimize-svgs', 'clean-images', 'copy-images', [ 'connect', 'watch' ], cb );
});

gulp.task( 'build', function( cb ) {
  return runSequence( 'clean', 'html', 'stats-analysis:scss-lint', 'scss', 'stats-analysis:retire', 'stats-analysis:js-lint', 'javascript', 'optimize-svgs', 'clean-images', 'copy-images', cb );
});

gulp.task( 'stats', function( cb ) {
  return runSequence( 'build', [ 'stats-analysis:cssstats', 'stats-analysis:audit', 'stats-analysis:size' ], [ 'stats-analysis:server:ngrok-mobile', 'stats-analysis:server:ngrok-desktop' ], [ 'stats-analysis:page-speed:mobile', 'stats-analysis:page-speed:desktop' ], ['connect'], cb );
});

gulp.task( 'audit', function( cb ) {
  return runSequence( 'stats-analysis:audit', cb );
});
