const webpack = require( 'webpack' );
const path = require( 'path' );

module.exports = {
  entry: './app/js/main.js',
  devtool: 'inline-source-map',
  output: {
    path: path.resolve( __dirname, '../build/js' ),
    filename: 'bundle.js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      enforce: 'pre',
      exclude: /node_modules/,
      use: [{
        loader: 'eslint-loader',
        options: {
          formatter: require( 'eslint-friendly-formatter' ),
          failOnError: true
        }
      }]
    }, {
      test: /\.js?$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: [
            'env',
            'stage-2'
          ],
          plugins: [
            require( 'babel-plugin-transform-runtime' )
          ]
        }
      }]
    }, {
      test: /\.modernizrrc.js$/,
      use: ['modernizr-loader']
    },
    {
      test: /\.modernizrrc(\.json)?$/,
      use: [
        'modernizr-loader',
        'json-loader'
      ]
    }]
  },
  resolve: {
    alias: {
      modernizr$: path.resolve( __dirname, '.modernizrrc' ),
      node_modules: path.resolve( __dirname, 'node_modules/' ),
      functions: path.resolve( __dirname, 'app/js/1-functions/' ),
      components: path.resolve( __dirname, 'app/components/6-components/' )
    }
  }
};


if ( process.env.NODE_ENV === 'production' ) {
  module.exports.plugins = [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.optimize.OccurenceOrderPlugin()
  ]
} else {
  module.exports.devtool = '#source-map'
}
