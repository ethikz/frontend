const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});
const webpack = require( 'webpack-stream' );


module.exports = ( gulp, cb ) => {
  return gulp.src( 'app/js/main.js' )
    .pipe( webpack( require( '../webpack.config.js' ) ) )
    .pipe( gulp.dest( 'build/js' ) )
    .pipe( plugin.connect.reload() )
    .on( 'error', mapError );
};
