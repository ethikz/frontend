const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});


module.exports = ( gulp, cb ) => {
  return gulp.src( [
    'build'
  ], {
    read: false
  })
    .pipe( plugin.clean() )
    .on( 'error', mapError );
};
