const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});
const op = require( 'openport' );


module.exports = ( gulp, cb ) => {
  op.find({
    startingPort: 3000,
    endingPort: 3050
  },
  ( err, port ) => {
    if ( err ) {
      mapError( err );
      return;
    }

    plugin.env.set({
      port
    });

    return plugin.connect.server({
      root: 'build',
      port,
      livereload: true
    });
  });
};
