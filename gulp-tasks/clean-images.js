const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});


module.exports = ( gulp, cb ) => {
  return gulp.src( 'build/images' )
    .pipe( plugin.clean() )
    .pipe( plugin.connect.reload() )
    .on( 'error', mapError );
};
