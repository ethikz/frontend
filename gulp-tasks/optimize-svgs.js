const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});

let paths = {
  images: 'app/images/svgs/**/*.svg'
};


module.exports = ( gulp, cb ) => {
  return gulp.src( paths.images )
    .pipe( plugin.svgmin() )
    .pipe( gulp.dest( 'build/images' ) )
    .pipe( plugin.connect.reload() )
    .on( 'error', mapError );
};
