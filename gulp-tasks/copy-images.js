const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});

let paths = {
  images: 'app/images/**'
};


module.exports = ( gulp, cb ) => {
  return gulp.src( paths.images )
    .pipe( gulp.dest( 'build/images' ) )
    .pipe( plugin.connect.reload() )
    .on( 'error', mapError );
};
