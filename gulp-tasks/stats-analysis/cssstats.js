const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});
const cssStats = require( 'gulp-cssstats' );
const os = require( 'os' );
const dateObj = new Date();
const browser = os.platform() === 'linux' ? 'google-chrome' : (
  os.platform() === 'darwin' ? 'google chrome' : (
    os.platform() === 'win32' ? 'chrome' : 'firefox' ) );
const newdate = `-${dateObj.getUTCMonth() + 1}-${dateObj.getUTCDate()}-${dateObj.getUTCFullYear()}-${dateObj.getHours()}-${dateObj.getMinutes()}-${dateObj.getSeconds()}`;


module.exports = ( gulp, cb ) => {
  return gulp.src( './build/css/main.css' )
    .pipe( cssStats({
      importantDeclarations: true,
      specificityGraph: true,
      sortedSelectors: true,
      uniqueColorsCount: true,
      displayNoneCount: true,
      sortedSpecificityGraph: true,
      repeatedSelectors: true,
      propertyResets: true,
      vendorPrefixedProperties: true
    }) )
    .pipe( plugin.rename({
      suffix: newdate
    }) )
    .pipe( gulp.dest( './stats' ) )
    .pipe( plugin.open({
      app: browser
    }) )
    .on( 'error', mapError );
};
