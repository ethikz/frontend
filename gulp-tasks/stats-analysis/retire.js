const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});


module.exports = ( gulp, cb ) => {
  return plugin.plumber({
    errorHandler( err ) {
      plugin.util.log( plugin.util.colors.red( 'Vulnerabilities have been found.  Check out reports/code-audit.json for details.' ) );
    }
  })
    .pipe( plugin.run( 'retire -c --path ./ --outputformat json --outputpath ./reports/code-audit.json --exitwith 0', {
      'verbosity': 0
    }).exec() )
    .pipe( plugin.plumber.stop() );
};
