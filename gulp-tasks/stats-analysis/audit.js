const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});


module.exports = ( gulp, cb ) => {
  return plugin.plumber({
    errorHandler( err ) {
      plugin.util.log( plugin.util.colors.red( 'Vulnerabilities have been found.  Check out reports/package-audit.xml for details.' ) );
    }
  })
    .pipe( plugin.run( 'auditjs -r -q', {
      'verbosity': 0
    }).exec() )
    .on( 'end', () => gulp.src( './reports/scan_node_modules.xml' )
      .pipe( plugin.rename( 'package-audit.xml' ) )
      .pipe( gulp.dest( './reports' ) ) )
    .pipe( plugin.plumber.stop() );
};
