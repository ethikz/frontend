const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});

let paths = {
  scss: 'app/**/*.scss'
};


module.exports = ( gulp, cb ) => {
  return gulp.src( paths.scss )
    .pipe( plugin.scssLint({
      'config': '.scss-lint.yml',
      'maxBuffer': 307200,
      'filePipeOutput': 'scss-lint-report.xml'
    }) )
    .pipe( gulp.dest( './reports' ) )
    .pipe( plugin.connect.reload() )
    .on( 'error', mapError );
};
