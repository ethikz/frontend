const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});

let paths = {
  js: 'app/js/**/*.js'
};


module.exports = ( gulp, cb ) => {
  return gulp.src( [
    'app/js/**/*.js',
    'gulp-tasks/**/*.js'
  ] )
    .pipe( plugin.jshint() )
    .pipe( plugin.jshint.reporter( 'jshint-stylish' ) )
    .pipe( plugin.eslint() )
    .pipe( plugin.eslint.format() )
    .pipe( plugin.eslint.failAfterError() )
    .pipe( plugin.connect.reload() )
    .on( 'error', mapError );
};
