const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});
const fs = require( 'fs' );
const psi = require( 'psi' );
const config = JSON.parse( fs.readFileSync( './gulp-config.json' ) );


module.exports = ( gulp, cb ) => {
  if ( plugin.run( 'pgrep -f ngrok' != '' ) ) {
    config.ngrokRunning = true;
    fs.writeFileSync( './gulp-config.json', JSON.stringify( config, null, ' ' ) );
  } else {
    plugin.runSequence( 'stats-analysis:server:desktop' );
  }

  plugin.util.log( plugin.util.colors.yellow( config.ngrokMobile ) );

  return psi( config.ngrokDesktop, {
    nokey: 'true',
    strategy: 'desktop'
  }).then( ({
    ruleGroups
  }) => {
    plugin.util.log( plugin.util.colors.green( `Speed score: ${ruleGroups.SPEED.score}` ) );
  });
};
