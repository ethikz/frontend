const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});
const fs = require( 'fs' );
const ngrok = require( 'ngrok' );
const config = JSON.parse( fs.readFileSync( './gulp-config.json' ) );


module.exports = ( gulp, cb ) => {
  return ngrok.connect({
    proto: 'http',
    addr: config.port
  }, ( err, url ) => {
    if ( err ) {
      plugin.util.log( plugin.util.colors.red( err ) );
    }

    config.ngrokDesktop = url;

    fs.writeFileSync( './gulp-config.json', JSON.stringify( config, null, ' ' ) );

    plugin.util.log( `Desktop tunnel is: ${plugin.util.colors.green( config.ngrokDesktop )}` );
    plugin.util.log( plugin.util.colors.yellow( 'Starting mobile Page Speed Insights' ) );
  });
};
