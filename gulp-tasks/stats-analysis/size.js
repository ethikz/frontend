const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});


module.exports = ( gulp, cb ) => {
  return gulp.src( './build/**/*' )
    .pipe( plugin.plumber({
      errorHandler( err ) {
        plugin.util.log( plugin.util.colors.red( err ) );
      }
    }) )
    .pipe( plugin.size() )
    .pipe( plugin.plumber.stop() )
    .on( 'end', () => plugin.util.log( plugin.util.colors.yellow( 'Starting mobile tunnel through ngrok' ) ) );
};
