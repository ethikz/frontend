let paths = {
  components: 'app/components/**/*',
  html: 'app/views/**/*.hbs',
  scss: 'app/scss/**/*.scss',
  images: 'app/images/**',
  js: 'app/js/**/*.js'
};


module.exports = ( gulp, cb ) => {
  gulp.watch( [ paths.html, `${paths.components}.hbs` ], ['html'] );
  gulp.watch( [ paths.scss, `${paths.components}.scss` ], [ 'scss', 'stats-analysis:scss-lint' ] );
  gulp.watch( paths.images, [ 'optimize-svgs', 'clean-images', 'copy-images' ] );
  gulp.watch( [ paths.js, `${paths.components}.js` ], [ 'stats-analysis:retire', 'stats-analysis:js-lint', 'javascript' ] );
};
