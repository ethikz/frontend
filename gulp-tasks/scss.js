const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});

let paths = {
  scss: 'app/scss/main.scss'
};


module.exports = ( gulp, cb ) => {
  return gulp.src( paths.scss )
    .pipe( plugin.sassGlob() )
    .pipe( plugin.sass() )
    .pipe( plugin.sourcemaps.init() )
    .pipe( plugin.autoprefixer( 'last 2 versions' ) )
    .pipe( plugin.cleanCss() )
    .pipe( plugin.sourcemaps.write( '.' ) )
    .pipe( gulp.dest( 'build/css' ) )
    .pipe( plugin.connect.reload() )
    .on( 'error', mapError );
};
