const plugin = require( 'gulp-load-plugins' )({
  camelize: true
});
const hb = require( 'gulp-hb' );
const rename = require( 'gulp-rename' );
const path = require( 'path' );

let paths = {
  html: 'app/views/**/*.hbs'
};


module.exports = ( gulp, cb ) => {
  return gulp.src( paths.html )
    .pipe( hb({
      bustCache: true
    })
      .partials( './app/components/**/*.{hbs, js}', {
        base: path.join( __dirname, '../app/components' ),
        parsePartialName( options, file ) {
          return path.parse( file.path ).name;
        }
      })
      .partials( './app/views/partials/**/*.{hbs, js}', {
        base: path.join( __dirname, '../app/views/partials' )
      })
    )
    .pipe( rename({
      extname: '.html'
    }) )
    .pipe( gulp.dest( './build' ) )
    .pipe( plugin.connect.reload() )
    .on( 'error', mapError );
};
